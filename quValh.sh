#!/bin/bash
set -euo pipefail

# keep this simplistic - used as folder name, service name etc
GAMESHORT="Valheim"
GAMEDIR="$HOME/${GAMESHORT}"
GAMEID=896660
GAMEBINDIR=$GAMEDIR
GAMEBIN=$GAMEDIR/valheim_server.x86_64
if [[ -f "$GAMEDIR/start_server_bepinex.sh" ]]; then
    GAMEBIN=$GAMEDIR/start_server_bepinex.sh
fi

_SELF=$(readlink -f "$0")
QUDIR=$(dirname "$_SELF")

GAMEOPTS="-logfile output.log -port 2456 -nographics -batchmode -public 1"
if [[ -f "$QUDIR/private" ]]; then
    # read custom settings
    source "$QUDIR/private"
fi


action_install() {
	# $ useradd steam; su steam -c this-script

	if [[ ! -d "$HOME/Steam" ]]; then
		mkdir $HOME/Steam

		cd $HOME/Steam/
		curl -sqL 'https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz' | tar zxvf -
	fi

	mkdir -p $GAMEDIR
	if [[ ! -f "$GAMEBIN" ]]; then
		set -x
		$HOME/Steam/steamcmd.sh +login anonymous +force_install_dir $GAMEDIR +app_update $GAMEID validate +quit
	fi

    action_service_update

    action_config_update
}

action_update() {
    $HOME/Steam/steamcmd.sh +login anonymous +force_install_dir $GAMEDIR +app_update $GAMEID validate +quit
}

action_config_check() {
    :
}
action_config_update() {
    action_config_check
}

action_service_update() {
    tee ${GAMEDIR}/${GAMESHORT}.service <<EOF
[Unit]
Description=${GAMESHORT}
Wants=network-online.target
After=syslog.target network.target nss-lookup.target network-online.target

[Service]
ExecStart=${_SELF} start
LimitNOFILE=100000
LimitCORE=0
User=steam
Group=steam

KillMode=mixed
KillSignal=SIGINT

[Install]
WantedBy=multi-user.target
EOF
## ExecStop=${_SELF} stop
}

action_start() {
    action_update
    action_config_update

    OPTS="${OPTS:-}"
    OPTS="${OPTS} ${GAMEOPTS}"
    cd $GAMEBINDIR
    ulimit -c 0
    export LD_LIBRARY_PATH=./linux64
    exec $GAMEBIN $GAMEOPTS
    #exec $GAMEBIN $OPTS
}

action_stop() {
	echo "not implemented"
	:
}

case "${1:-start}" in
    install)
        action_install
        exit
        ;;
    update)
        action_stop
        action_update
        exit
        ;;
    start)
        action_start
        ;;
    stop)
        action_stop
        ;;
    restart)
        action_stop
        action_start
        ;;
    *)
        echo "Unrecognized action $1, supported are: install, update, start, stop, restart" >&2
        exit 1
esac

