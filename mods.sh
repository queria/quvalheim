#!/bin/sh

# ./mods.sh [-v] [--update] <path_to_valheim_dir>
# Example usage:
# ./mods.sh -v /home/steam/Valheim/
# ./mods.sh -v --update ~/.local/share/Steam/steamapps/common/Valheim/

set -eo pipefail

VERBOSE_OPT=""
if [[ "$1" == "-v" ]]; then
    shift
    VERBOSE_OPT="-v"
    set -x
fi

UPDATE_OPT=""
if [[ "$1" == "--update" ]]; then
    shift
    UPDATE_OPT="update"
fi


GAME_DIR=${GAME_DIR:-$1}
GAME_DIR=${GAME_DIR%/}
GAME_DIR=${GAME_DIR:-.}

DATA=""
if [[ -d "${GAME_DIR}/valheim_Data" ]]; then
    DATA=client
elif [[ -d "${GAME_DIR}/valheim_server_Data" ]]; then
    DATA=server
else
    echo "Specify path to Valheim game directory as first param"
    exit 1
fi

BEPINEX_URL="${BEPINEX_URL:-https://github.com/BepInEx/BepInEx/releases/download/v5.4.22/BepInEx_unix_5.4.22.0.zip}"

function fetchMod {
    local pkgName="$1"
    local pkgFile="${pkgName//\//_}"

    if [[ -f "$pkgFile" ]]; then
        if [[ -z "$UPDATE_OPT" ]]; then
            return
        fi
        rm -f "$pkgFile"
    fi

    if [[ "$pkgName" == "BepInEx" ]]; then
        local pkgDownloadUrl="${BEPINEX_URL}"
    else
        local pkgQueryUrl="https://valheim.thunderstore.io/api/experimental/package/${pkgName}/"
        local pkgDownloadUrl="$(curl --fail --retry 2 "$pkgQueryUrl" | jq -r .latest.download_url)"
    fi

    if [[ -z "$pkgDownloadUrl" ]]; then
        echo "ERROR: unable to query download url for $pkgName" >&2
        exit 1
    fi
    if ! curl --fail --retry 2 --retry-all-errors -L -o "$pkgFile" "$pkgDownloadUrl"; then
        rm -f "$pkgFile"
        echo "ERROR: failed to download $pkgName"
        exit 1
    fi
}

function unpackMod {
    local pkgName="$1"
    local pkgFile="${pkgName//\//_}"
    local pkgDir="_${pkgFile}"

    fetchMod "$pkgName"
    if [[ ! -d "${pkgDir}" || -n "$UPDATE_OPT" ]]; then
        mkdir -p "${pkgDir}"
        cd "${pkgDir}"
        if ! 7z x -aoa ../$pkgFile; then
            echo "ERROR: failed to unpack $pkgName mod"
            cd ..
            # rm -rf "${pkgDir}"
            exit 1
        fi
        cd ..
    fi
}

function installMod {
    local pkgName="$1"
    local pkgFile="${pkgName//\//_}"
    local pkgDir="_${pkgFile}"

    unpackMod "$pkgName"
    find "${pkgDir}" -iname \*.dll -exec cp ${VERBOSE_OPT} {} ${GAME_DIR}/BepInEx/plugins/ \;
    #rsync -v "${pkgDir}/"*.dll "${GAME_DIR}/BepInEx/plugins/" || \
    #rsync -v "${pkgDir}/plugins/"*.dll "${GAME_DIR}/BepInEx/plugins/"
}

function check_configs_safe {
    # we only care in case of updating,
    # as then we are going to overwrite the files
    if [[ -n "$UPDATE_OPT" ]]; then
        local cfg="${GAME_DIR}/BepInEx/config"
        # config dir exists and its git status is clean
        # (incl. errors from git like not git repo,
        #  as that means configs could be lost)
        if [[ -d "$cfg" && "" != "$(git -C "$cfg" status --porcelain 2>&1)" ]]; then
            echo "ERROR: Config dir has changes not stored in git, would be lost during update" >&2
            echo "Config dir: $cfg" >&2
            exit 1
        fi
  fi
}

mkdir -p "${GAME_DIR}/__mods"
cd "${GAME_DIR}/__mods"

check_configs_safe

# download and unpack ...
unpackMod "BepInEx"


# unpackMod "denikson/BepInExPack_Valheim"
# ... and for BepInEx itself do custom install

if [[ ! -d "${GAME_DIR}/BepInEx" || -n "$UPDATE_OPT" ]]; then
    rsync -r "${GAME_DIR}/__mods/_BepInEx/" "${GAME_DIR}"
    chmod 755 "${GAME_DIR}/BepInEx"
    chmod +x "${GAME_DIR}/run_bepinex.sh"
    # sed -ri 's/executable_name=""/executable_name="valheim_server.x86_64"/' "${GAME_DIR}/run_bepinex.sh"
    # mv "${GAME_DIR}/run_bepinex.sh" "${GAME_DIR}/start_server_bepinex.sh"
    echo '#!/bin/sh' > "${GAME_DIR}/start_server_bepinex.sh"
    echo './run_bepinex.sh ./valheim_server.x86_64 "$@"' >> "${GAME_DIR}/start_server_bepinex.sh"
    chmod +x "${GAME_DIR}/start_server_bepinex.sh"

    #chmod +x "${GAME_DIR}/start_game_bepinex.sh"
    #chmod +x "${GAME_DIR}/start_server_bepinex.sh"
fi

installMod "ValheimModding/Jotunn"

installMod "GemHunter1/NoAFKEvents"
installMod "Trinsic/Death_Tweaks"
installMod "MSchmoecker/MultiUserChest"
installMod "tonsit/InteractWhileBuilding"
installMod "TastyChickenLegs/BetterCarts"
installMod "NexusTransfer/TopMining"

## plant anything (compat with planteasily)
# https://valheim.thunderstore.io/package/Advize/PlantEverything/
installMod "Advize/PlantEverything"
# https://valheim.thunderstore.io/package/Advize/PlantEasily/
installMod "Advize/PlantEasily"
#
## item chests showing the icon
# https://valheim.thunderstore.io/package/makail/ItemDrawers/
installMod "makail/ItemDrawers"
#
##
# https://valheim.thunderstore.io/package/abdcef/AmmoInfoDisplay/
installMod "abdcef/AmmoInfoDisplay"
#
## hold mouse button for repeated action
installMod "KxEdna/aedenthorn_HoldRepeatActions"
#
## quick join server
# https://valheim.thunderstore.io/package/bdew/QuickConnect/
installMod "bdew/QuickConnect"
# https://valheim.thunderstore.io/package/Azumatt/FastLink/
#
## https://valheim.thunderstore.io/package/Smoothbrain/Vitality/
installMod "Smoothbrain/Vitality"
#
## first person view
installMod "Masa/FirstPerson"
#
## quickly store items
installMod "Goldenrevolver/Quick_Stack_Store_Sort_Trash_Restock"
#
## auto-fuel campfires/kilns/smelters/... from nearby chests/ground
installMod "TastyChickenLegs/AutomaticFuel"
#
## optimize resource unloading, not enabled by default (varying results), enable for one-time install
#installMod "Azumatt/ResourceUnloadOptimizer"
#

##### MAYBE LATER
#
## feed animals from nearby chest (feed through)
# https://valheim.thunderstore.io/package/LordJay/Auto_Feed/
# https://valheim.thunderstore.io/package/Gizmorphium/AutoFeedWithContainerFilter/
#
## likely not compat with planteverything/easily
# https://valheim.thunderstore.io/package/Smoothbrain/Farming/
# https://valheim.thunderstore.io/package/Smoothbrain/Foraging/
#
## shift + harvest/plant (5x5) - planteasily seems good enough
# https://valheim.thunderstore.io/package/Marlthon/GoodFarmer/
#
## AoE pickaxe
#installMod "chase000/AoeMining"
#

if [[ "$DATA" == "client" && ! -f "${GAME_DIR}/BepInEx/plugins/multiplayerboatdamage.dll" ]]; then
    echo -e "NOTICE: \033[00;31mDownload MultiplayerBoatDamage fix manually\033[0m"
    echo "        (yes it sucks bcs of nexusmod, as its not on thunderstore)"
    echo "        https://www.nexusmods.com/valheim/mods/579"
fi

set +x

echo ""
echo "Make sure You launch server via start_server_bepinex.sh (modify the opts)"
echo "Make sure You launch game via './start_game_bepinex.sh %command%'"
echo ""
